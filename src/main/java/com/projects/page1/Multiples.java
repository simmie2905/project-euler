package com.projects.page1;
import java.util.ArrayList;

public class Multiples {
//Find the sum of all the multiples of 3 or 5 below 1000.

    private static int lcm = 0;
    private static ArrayList<Integer> num1multiples = new ArrayList<>(), num2multiples = new ArrayList<>();

    public static void getMultiples(int num1, int num2, int limit)
    {
        int num1multiple = 0, num2multiple = 0;

        //num1 multiples
        for(int i = 0; i < limit/num1; i++)
        {
            num1multiple = num1multiple + num1;
            if(num1multiple != limit)
                num1multiples.add(num1multiple);
        }
        
        //num2 multiples
        for(int i = 0; i < limit/num2; i++)
        {
            num2multiple = num2multiple + num2;
            if(num2multiple != limit)
                num2multiples.add(num2multiple);
        }

//        System.out.println("Multiples of " + num1 + ":  " + num1multiples);
//        System.out.println("Multiples of " + num2 + ":  " + num2multiples);
    }

    //Finding the lowest common multiple
    public static int lcm(int num1, int num2)
    {
        getMultiples(num1, num2, 1000);

        for(int i = 1; i<num1multiples.size(); i++)
        {
            if(num2multiples.contains(num1multiples.get(i)))
            {
                lcm = num1multiples.get(i);
                break;
            }
        }

        System.out.println("\nLCM: "+ lcm);

        return lcm;
    }

    public static int sumOfMultiples(int num1, int num2, int limit)
    {
        lcm(num1, num2);

        ArrayList<Integer> lcmMultiples = new ArrayList<>();
        int lcmMultiple = 0;

        for(int i = 0; i < limit/lcm; i++)
        {
            lcmMultiple = lcmMultiple + lcm;
            if(lcmMultiple != limit)
                lcmMultiples.add(lcmMultiple);
        }
        System.out.println("Multiples of "+lcm+":  "+ lcmMultiples);

        int ans1 = sumOfMultiples(num1, limit);
        int ans2 = sumOfMultiples(num2, limit);
        int ans3 = sumOfMultiples(lcm, limit);
        
        int sum = ans1 + ans2 - ans3;
        System.out.println("Final sum = "+sum);
        return sum;
    }

    public static int sumOfMultiples(int num, int limit)
    {
        int numberOfTerms = (limit-1) / num;
        int sum = numberOfTerms * (numberOfTerms + 1) / 2;
        int sumOfTerms = num * sum;

        System.out.println("Sum of " + num + " multiples: " + sumOfTerms);
        return sumOfTerms;
    }

    public static int smallestMultiple(int max)
    {
        double tester = max;
        double number = 0.0;

        for(int i = 1; i <= max; i++)
        {
            number = tester / i;
            System.out.println(tester +" / " + i + " = " + number);
            if(number % 1 != 0)
            {
                System.out.println(">>>resetting tester");
                tester++;
                i = 0;
            }

        }

        return (int)tester;

    }

    public static void main(String [] args)
    {
//        int sum = Multiples.sumOfMultiples(3,5,10);
//        System.out.println(sum);

        Multiples.smallestMultiple(10);
    }
}


package com.projects.page1;

import java.util.ArrayList;
import java.util.Collections;

public class LargestProduct2 {
    private static int [][] grid = new int [6][10];
    private ArrayList<Integer> productValues = new ArrayList<>();

    private static int[][] init()
    {
        for(int row = 0; row < grid.length; row++)
        {
            for(int col = 0; col < grid[row].length; col++)
            {
                int random = (int)(Math.random() * 10);
                grid[row][col] = random;
            }
        }

        return grid;
    }

    public void display()
    {
        int[][] display = init();

        for(int row = 0; row < grid.length; row++)
        {
            for(int col = 0; col < grid[row].length; col++)
            {
                System.out.print(display[row][col] + "  ");
            }
            System.out.println("\n");
        }
    }

    public void findGreatestProduct()
    {
        int highestProduct,num1,num2,num3,num4 = 0;
        for (int[] ints : grid) {
            for (int col = 0; col < ints.length - 3; col++) {
                if (ints[col] != ints.length) {
                    num1 = ints[col];
                    //System.out.print(num1 + " ");
                    num2 = ints[col + 1];
                  //  System.out.print(num2 + " ");
                    num3 = ints[col + 2];
                 //   System.out.print(num3 + " ");
                    num4 = ints[col + 3];
                 //   System.out.print(num4 + " ");

                    highestProduct = num1 * num2 * num3 * num4;

                    productValues.add(highestProduct);
                }
            }
            //System.out.println("\n");
        }

        System.out.println("Vertical products");

        for (int row = 0; row < grid.length; row++)
        {
            for(int col = 0; col < grid[row].length; col++)
            {

                System.out.print(grid[row][col] + " ");
                //num2 = grid[col + 1];
                System.out.print(grid[row + 1][col] + " ");
                //num3 = grid[col + 2];
                //System.out.print(grid[row+2][col] + " ");
                //num4 = grid[col + 3];
                System.out.print("__");
            }


        }




        System.out.println("Products: "+productValues);

        Collections.sort(productValues);
        highestProduct = productValues.get(productValues.size()-1);
        System.out.println("The largest product is "+ highestProduct);
    }


    public static void main(String[] args) {
        LargestProduct2 lpf = new LargestProduct2();
        lpf.display();
        lpf.findGreatestProduct();
    }
}

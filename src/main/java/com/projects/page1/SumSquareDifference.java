package com.projects.page1;

import com.google.common.math.IntMath;

public class SumSquareDifference {
    //Find the difference between the sum of the squares of the first one hundred natural numbers and the square of the sum.

    public static int sumOfSquares(int max)
    {
        int answer = 0;

        for(int i = 1; i <= max; i++) { answer = answer + IntMath.pow(i, 2); }

        System.out.println("Sum of squares = "+ answer);

        return answer;
    }

    public static int squareOfSum(int max)
    {
        int answer = 0;

        for(int i = 1; i <= max; i++) { answer = answer + i; }

        System.out.println("Square of sum = " + answer);
        answer = IntMath.pow(answer, 2);

        return answer;
    }

    public static int sumSqrDiff(int squareOfSum, int sumOfSquare)
    {
        return IntMath.saturatedSubtract(squareOfSum, sumOfSquare);
    }

    public static void main(String[] args) {

        System.out.println("DIFFERENCE: "+ SumSquareDifference.sumSqrDiff(SumSquareDifference.squareOfSum(100), SumSquareDifference.sumOfSquares(100)));

    }
}

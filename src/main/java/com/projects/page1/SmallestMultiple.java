package com.projects.page1;

public class SmallestMultiple {

    private final int max;

    //2520 is the smallest number that can be divided by each of the numbers from 1 to 10 without any remainder.
    //What is the smallest positive number that is evenly divisible by all of the numbers from 1 to 20?

    public SmallestMultiple(int max)
    {
        this.max = max;
    }


    public void findMultiples()
    {
        int number = 0;
        for(int i = 0; i < max; i++)
        {
            number = 2520 * i;

            for(int j = 1; j <= max; j++)
            {
                if(number % j == 0)
                {
                    System.out.println(2520 + " is evenly divisible by "+ j);
                }
            }
        }

    }

    public static void main(String[] args) {

        SmallestMultiple smallestMultiple= new SmallestMultiple(20);
        smallestMultiple.findMultiples();
    }

}

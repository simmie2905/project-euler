package com.projects.page1;

import com.google.common.math.LongMath;

import java.util.ArrayList;
import java.util.Collections;

public class PrimeSummation {

    public static ArrayList<Integer> findPrimes(long number)
    {
        ArrayList<Integer> primeFactors = new ArrayList<>();
        //Getting all factors of the given number
        for(int i = 1; i < number; i++)
        {
            //System.out.println("\n"+num + "/" + i + " = " + factor+"\n");
            if(((double) i == 1.0) || ((double) i % 1 == 0))
            {
                //System.out.println(factor + " is a whole number.");
                if(LongMath.isPrime((int) (double) i)) { primeFactors.add((int) (double) i); }
            }
        }
        Collections.sort(primeFactors);
        System.out.println(number + " prime factors: "+primeFactors);
        return primeFactors;
    }

    public int addPrimes(ArrayList<Integer> primeNumbers)
    {
        int sum = 0;

        for (Integer primeNumber : primeNumbers) {
            sum = sum + primeNumber;
        }

        return sum;
    }

    public static void main(String[] args) {
        PrimeSummation primeSummation = new PrimeSummation();
        System.out.println("SumOfPrimes: "+primeSummation.addPrimes(findPrimes(2000000)));
    }
}

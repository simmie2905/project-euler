package com.projects.page1;
import com.google.common.math.LongMath;
import java.util.ArrayList;
import java.util.Collections;

public class PrimeFactor {

    //The prime factors of 13195 are 5, 7, 13 and 29.
    //What is the largest prime factor of the number 600851475143?
    //How to find a prime number
    private long number = 0;

    public PrimeFactor(long number)
    {
        this.number = number;
    }

    public static ArrayList<Integer> getPrimeFactors(long number)
    {
        if(number <= 1)
        {
            System.out.println(number + " DOES NOT HAVE A PRIME NUMBER");
            System.exit(0);
        }

        ArrayList<Integer> primeFactors = new ArrayList<>();

        //Getting all factors of the given number
        for(int i = 1; i < (double)number; i++)
        {
            double factor = (double)number / i;
            //System.out.println("\n"+num + "/" + i + " = " + factor+"\n");
            if(factor % 1 == 0)
            {
                //System.out.println(factor + " is a whole number.");
                if(LongMath.isPrime((int)factor)) { primeFactors.add((int)factor); }
            }
        }
        Collections.sort(primeFactors);
        System.out.println(number + " prime factors: "+primeFactors);

        return primeFactors;
    }

    public int getLargestPrimeFactor()
    {
        ArrayList<Integer> primeFactors = getPrimeFactors(number);
        return primeFactors.get(primeFactors.size()-1);
    }

    public static int getNthPrimeFactor(int n)
    {
        ArrayList<Integer> primeNumbers = new ArrayList<>();

        for(int i = 1; primeNumbers.size() < n; i++)
        {
            if(LongMath.isPrime(i))
            {
                primeNumbers.add(i);
            }
        }
        Collections.sort(primeNumbers);
        int nthPrime = primeNumbers.get(primeNumbers.size() -1);
        return nthPrime;
    }

    public static void main(String[] args) {

        PrimeFactor primeFactor = new PrimeFactor(600851475143L);
        int lpf = primeFactor.getLargestPrimeFactor();
        System.out.println(lpf);




    }


}

package com.projects.page1;
public class FibonacciEvenNumbers {

    //By considering the terms in the Fibonacci sequence whose values do not exceed four million, find the sum of the even-valued terms
    private int a = 0, b = 1, c = 0, sum = 0;

    public void addEven(int max)
    {
        while(c < max)
        {
            c = a + b;
            if(c % 2 == 0)
            {
                System.out.println(c);
                sum = sum + c;
            }
            a = b;
            b = c;
        }
        System.out.println("SUM: "+sum);
    }

    public void addOdd(int max)
    {

    }

    public static void main(String[] args) {
        FibonacciEvenNumbers fibonacciEvenNumbers = new FibonacciEvenNumbers();
        fibonacciEvenNumbers.addEven(4000000);
    }
}

package com.projects.page1;

import java.util.ArrayList;

public class PalindromeProduct {

//Question: Find the largest palindrome made from the product of two 3-digit numbers.

        private ArrayList<Integer> arr1 = new ArrayList<>();
        private ArrayList<Integer> arr2 = new ArrayList<>();

        public PalindromeProduct(int numOfDigits)
        {
                for(int i = 10; i<=99; i++)
                {
                        arr1.add(i);
                        arr2.add(i);
                }

                System.out.println(arr1);
        }

        public void palindrome()
        {
                System.out.println("In palindrome method");
                for(int i = 0; i<arr1.size(); i++)
                {
                        for(int j = 0; j<arr2.size(); j++)
                        {
                                int palindromeProduct = arr1.get(i) * arr2.get(j);
                                int reversedInteger = 0, remainder, originalInteger;
                                originalInteger = palindromeProduct;
                                // reversed integer is stored in variable
                                while( palindromeProduct != 0 )
                                {
                                        remainder = palindromeProduct % 10;
                                        reversedInteger = reversedInteger * 10 + remainder;
                                        palindromeProduct  /= 10;
                                }
                                // palindrome if original Integer and reversedInteger are equal
                                if (originalInteger == reversedInteger)
                                        System.out.println(originalInteger + " is a palindrome.      "+ arr1.get(i)+" * "+arr2.get(j));

                        }
                }
        }

        public static void main(String[] args) {
                PalindromeProduct palindromeProduct = new PalindromeProduct(2);
                palindromeProduct.palindrome();
        }

}

import com.projects.page1.Multiples;
import org.junit.Test;
import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;

import static org.junit.Assert.*;

public class TestJunit {
    String temp = "hello world";

   @Test
   public void testMultiplesClass ()
   {
       assertEquals(15, Multiples.lcm(5,3));
       assertEquals(166833, Multiples.sumOfMultiples(3, 1000));
   }


    public static void main(String[] args) {

       Result result = JUnitCore.runClasses(TestJunit.class);

       for(Failure failure : result.getFailures())
       {
           System.out.println(failure.toString());
       }

        System.out.println(result.wasSuccessful());
    }
}
